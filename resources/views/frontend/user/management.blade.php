@extends('frontend.layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Users
                        <a class="btn btn-success pull-right" href="{{ route('frontend.user.create.form') }}">Create</a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="users-table" class="table table-condensed table-hover">
                                <thead>
                                <tr>
                                    <th>{{ trans('labels.backend.access.users.table.id') }}</th>
                                    <th>{{ trans('labels.backend.access.users.table.name') }}</th>
                                    <th>{{ trans('labels.backend.access.users.table.email') }}</th>
                                    <th>{{ trans('labels.backend.access.users.table.confirmed') }}</th>
                                    <th>{{ trans('labels.backend.access.users.table.roles') }}</th>
                                    <th>{{ trans('labels.backend.access.users.table.created') }}</th>
                                    <th>{{ trans('labels.backend.access.users.table.last_updated') }}</th>
                                    <th>{{ trans('labels.general.actions') }}</th>
                                </tr>
                                </thead>
                            </table>
                        </div><!--table-responsive-->
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('after-scripts-end')
    {{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/backend/plugin/datatables/dataTables.bootstrap.min.js") }}

    <script>
        $(function() {
            company = $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("frontend.user.data") }}',
                    type: 'get',
                    data: function (d) {
                        d.company = $('#company-select').val();
                        d.status = 1;
                        d.trashed = false;
                    }
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'confirmed', name: 'confirmed'},
                    {data: 'roles', name: 'roles',searchable:true},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'actions', name: 'actions'}
                ],
                order: [[0, "asc"]],
                searchDelay: 500
            });
        });
        $(document).ready(function () {
            $('#company-select').on('change', function () {
                company.draw();
            });
        });
    </script>
@stop