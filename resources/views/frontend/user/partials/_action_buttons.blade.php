<td>
    <a href="{{ route('frontend.user.edit',$user->id) }}" class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i></a>
    @if($user->id != access()->user()->id)
        @if($user->confirmed)
            <a href="{{ route('frontend.user.deactivate',$user->id) }}" class="btn btn-xs btn-warning"><i class="fa fa-pause" data-toggle="tooltip" data-placement="top" title="Deactivate"></i></a>
        @else
            <a href="{{ route('frontend.user.activate',$user->id) }}" class="btn btn-xs btn-warning"><i class="fa fa-play" data-toggle="tooltip" data-placement="top" title="Activate"></i></a>
        @endif
        <a href="{{ route('frontend.user.delete',$user->id) }}" class="btn btn-xs btn-danger"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>
    @else
        <a href="{{ route('auth.password.change') }}" class="btn btn-xs btn-info"><i class="fa fa-refresh" data-toggle="tooltip" data-placement="top" title="Change Password"></i></a>
    @endif
</td>