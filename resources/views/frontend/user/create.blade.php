@extends('frontend.layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Create User
                    </div>
                    <div class="panel-body">
                        {{ Form::open(['route' => 'frontend.user.create', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}

                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">{{ trans('labels.backend.access.users.create') }}</h3>
                            </div><!-- /.box-header -->

                            <div class="box-body">
                                <div class="form-group">
                                    {{ Form::label('name', trans('validation.attributes.backend.access.users.name'), ['class' => 'col-lg-3 control-label']) }}

                                    <div class="col-lg-9">
                                        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.users.name')]) }}
                                    </div><!--col-lg-9-->
                                </div><!--form control-->

                                <div class="form-group">
                                    {{ Form::label('email', trans('validation.attributes.backend.access.users.email'), ['class' => 'col-lg-3 control-label']) }}

                                    <div class="col-lg-9">
                                        {{ Form::text('email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.users.email')]) }}
                                    </div><!--col-lg-9-->
                                </div><!--form control-->

                                <div class="form-group">
                                    {{ Form::label('password', trans('validation.attributes.backend.access.users.password'), ['class' => 'col-lg-3 control-label', 'placeholder' => trans('validation.attributes.backend.access.users.password')]) }}

                                    <div class="col-lg-9">
                                        {{ Form::password('password', ['class' => 'form-control']) }}
                                    </div><!--col-lg-9-->
                                </div><!--form control-->

                                <div class="form-group">
                                    {{ Form::label('password_confirmation', trans('validation.attributes.backend.access.users.password_confirmation'), ['class' => 'col-lg-3 control-label', 'placeholder' => trans('validation.attributes.backend.access.users.password_confirmation')]) }}

                                    <div class="col-lg-9">
                                        {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
                                    </div><!--col-lg-9-->
                                </div><!--form control-->

                                <div class="form-group">
                                    {{ Form::label('status', trans('validation.attributes.backend.access.users.active'), ['class' => 'col-lg-3 control-label']) }}

                                    <div class="col-lg-1">
                                        {{ Form::checkbox('status', '1', true) }}
                                    </div><!--col-lg-1-->
                                </div><!--form control-->

                                <div class="form-group">
                                    {{ Form::label('confirmed', trans('validation.attributes.backend.access.users.confirmed'), ['class' => 'col-lg-3 control-label']) }}

                                    <div class="col-lg-1">
                                        {{ Form::checkbox('confirmed', '1', true) }}
                                    </div><!--col-lg-1-->
                                </div><!--form control-->

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">{{ trans('validation.attributes.backend.access.users.send_confirmation_email') }}<br/>
                                        <small>{{ trans('strings.backend.access.users.if_confirmed_off') }}</small>
                                    </label>

                                    <div class="col-lg-1">
                                        {{ Form::checkbox('confirmation_email', '1') }}
                                    </div><!--col-lg-1-->
                                </div><!--form control-->
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">{{ trans('validation.attributes.frontend.company') }}</label>
                                    <div class="col-lg-3">
                                        @if ($user->companies)
                                            @foreach($user->companies as $company)
                                                <input type="checkbox" value="{{ $company->id }}" name="companies[]" id="role-{{ $company->id }}"/>
                                                <label for="role-{{ $company->id }}">{{ $company->name }}</label>
                                                <br>
                                            @endforeach
                                        @else
                                            {{ trans('labels.backend.access.users.no_roles') }}
                                        @endif
                                    </div><!--col-lg-3-->
                                </div><!--form control-->
                                <div class="form-group">
                                    {{ Form::label('status', trans('validation.attributes.backend.access.users.associated_roles'), ['class' => 'col-lg-3 control-label']) }}

                                    <div class="col-lg-3">
                                        @if ($roles)
                                            @foreach($roles as $role)
                                                <input type="checkbox" value="{{ $role->id }}" name="roles[]" id="role-{{ $role->id }}" /> <label for="role-{{ $role->id }}">{{ $role->name }}</label>
                                                <a href="#" data-role="role_{{ $role->id }}" class="show-permissions small">
                                                    (
                                                    <span class="show-text">{{ trans('labels.general.show') }}</span>
                                                    <span class="hide-text hidden">{{ trans('labels.general.hide') }}</span>
                                                    {{ trans('labels.backend.access.users.permissions') }}
                                                    )
                                                </a>
                                                <br/>
                                                <div class="permission-list hidden" data-role="role_{{ $role->id }}">
                                                    @if ($role->all)
                                                        {{ trans('labels.backend.access.users.all_permissions') }}<br/><br/>
                                                    @else
                                                        @if (count($role->permissions) > 0)
                                                            <blockquote class="small">{{--
                                        --}}@foreach ($role->permissions as $perm){{--
                                            --}}{{$perm->display_name}}<br/>
                                                                @endforeach
                                                            </blockquote>
                                                        @else
                                                            {{ trans('labels.backend.access.users.no_permissions') }}<br/><br/>
                                                        @endif
                                                    @endif
                                                </div><!--permission list-->
                                            @endforeach
                                        @else
                                            {{ trans('labels.backend.access.users.no_roles') }}
                                        @endif
                                    </div><!--col-lg-3-->
                                </div><!--form control-->
                            </div><!-- /.box-body -->
                        </div><!--box-->

                        <div class="box box-info">
                            <div class="box-body">
                                <div class="pull-left">
                                    {{ link_to_route('frontend.user.management', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                                </div><!--pull-left-->

                                <div class="pull-right">
                                    {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success btn-xs']) }}
                                </div><!--pull-right-->

                                <div class="clearfix"></div>
                            </div><!-- /.box-body -->
                        </div><!--box-->

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('after-scripts-end')
    {{ Html::script('js/backend/access/users/script.js') }}
@stop
