@extends('frontend.layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ trans('exceptions.backend.access.companies.edit') }}
                    </div>
                    <div class="panel-body">
                        {{ Form::model($company,['route' => 'frontend.company.update', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
                        {{ Form::hidden('id',$company->id) }}
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">{{ trans('exceptions.backend.access.companies.edit') }}</h3>
                            </div><!-- /.box-header -->

                            <div class="box-body">
                                <div class="form-group">
                                    {{ Form::label('name', trans('validation.attributes.backend.access.users.name'), ['class' => 'col-lg-3 control-label']) }}

                                    <div class="col-lg-9">
                                        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.users.name')]) }}
                                    </div><!--col-lg-9-->
                                </div><!--form control-->
                            </div><!-- /.box-body -->
                        </div><!--box-->

                        <div class="box box-info">
                            <div class="box-body">
                                <div class="pull-left">
                                    {{ link_to_route('frontend.company.management', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                                </div><!--pull-left-->

                                <div class="pull-right">
                                    {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-success btn-xs']) }}
                                </div><!--pull-right-->

                                <div class="clearfix"></div>
                            </div><!-- /.box-body -->
                        </div><!--box-->

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('after-scripts-end')
    {{ Html::script('js/backend/access/users/script.js') }}
@stop
