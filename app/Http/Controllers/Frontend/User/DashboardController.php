<?php

namespace App\Http\Controllers\Frontend\User;

use App\Events\Frontend\Auth\UserRegistered;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Access\Company\Company;
use App\Models\Access\User\User;
use App\Repositories\Backend\Access\Role\RoleRepositoryContract;
use App\Repositories\Frontend\Access\User\UserRepositoryContract;
use Yajra\Datatables\Datatables;
use App\Http\Requests\Frontend\User\ManageUserRequest;
use App\Http\Requests\Frontend\User\StoreUserRequest;
use App\Http\Requests\Frontend\User\UpdateUserRequest;
use App\Http\Requests\Frontend\Company\CompanyRequest;
use App\Http\Requests\Frontend\User\SetActiveRequest;
/**
 * Class DashboardController
 * @package App\Http\Controllers\Frontend
 */
class DashboardController extends Controller
{
    /**
     * @var UserRepositoryContract
     */
    protected $users;

    /**
     * @var RoleRepositoryContract
     */
    protected $roles;

    public function __construct(UserRepositoryContract $users, RoleRepositoryContract $roles)
    {
        $this->users = $users;
        $this->roles = $roles;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('frontend.user.dashboard')
            ->withUser(access()->user());
    }

    public function users()
    {
        return view('frontend.user.management');
    }

    public function getUsers(ManageUserRequest $request)
    {
        return Datatables::of($this->users->getForDataTable($request->get('status'), access()->user(),$request->get('company')))
            ->editColumn('confirmed', function($user) {
                return $user->confirmed_label;
            })
            ->addColumn('roles', function($user) {
                $roles = [];

                if ($user->roles()->count() > 0) {
                    foreach ($user->roles as $role) {
                        array_push($roles, $role->name);
                    }

                    return implode("<br/>", $roles);
                } else {
                    return trans('labels.general.none');
                }
            })
            ->addColumn('actions', function($user) {

                return view('frontend.user.partials._action_buttons',compact('user'));
            })
            ->make(true);

    }

    public function create()
    {
        $roles = $this->users->getManagerRoles();
        return view('frontend.user.create',compact('roles'))->withUser(access()->user());
    }

    public function store(StoreUserRequest $request)
    {
        $user = $this->users->createManagerUser($request->all());
        return redirect()->route('frontend.user.management')->withFlashSuccess(trans('exceptions.frontend.auth.confirmation.created_confirm'));
    }

    public function edit(User $user)
    {
        $roles = $this->users->getManagerRoles();
        return view('frontend.user.edit',compact('roles'))->withUser($user);
    }
    public function update(UpdateUserRequest $request)
    {
        $user = $this->users->updateManagerUser($request->all());
        return redirect()->route('frontend.user.management')->withFlashSuccess(trans('exceptions.frontend.auth.confirmation.updated'));
    }

    public function deactivate(User $user)
    {
        $user = $this->users->deactivate($user);
        return redirect()->route('frontend.user.management')->withFlashSuccess(trans('exceptions.frontend.auth.deactivated'));
    }
    public function activate(User $user)
    {
        $user = $this->users->activate($user);
        return redirect()->route('frontend.user.management')->withFlashSuccess(trans('exceptions.frontend.auth.activated'));
    }

    public function delete(User $user)
    {
        $user = $this->users->delete($user);
        return redirect()->route('frontend.user.management')->withFlashSuccess(trans('exceptions.frontend.auth.deleted'));
    }

    public function setActiveCompany(SetActiveRequest $request)
    {
        $user = User::find(access()->user()->id);
        $user->active_company = $request->get('id');
        $user->save();
        return 'ok';
    }
    // Companies
    public function companies()
    {
        return view('frontend.company.management');
    }

    public function companyData()
    {
        return Datatables::of(Company::select(['*'])->whereHas('users',function($query) {
            return $query->where('users.id',access()->user()->id);
        })->get())
            ->addColumn('actions', function($row) {
                return view('frontend.company.partials._action_buttons',compact('row'));
            })
            ->make(true);
    }

    public function companyCreate()
    {
        return view('frontend.company.create');
    }

    public function companyStore(CompanyRequest $request)
    {
        $company = new Company();
        $created = $company->create($request->all());
        $created->users()->attach(access()->user()->id);
        return redirect()->route('frontend.company.management')->withFlashSuccess(trans('exceptions.backend.access.companies.created'));
    }

    public function companyEdit(Company $company)
    {
        return view('frontend.company.edit',compact('company'));
    }

    public function companyUpdate(CompanyRequest $request)
    {
        $company = Company::find($request->get('id'));
        $company->update($request->all());
        return redirect()->route('frontend.company.management')->withFlashSuccess(trans('exceptions.backend.access.companies.edited'));
    }

    public function companyDelete(Company $company)
    {
        $company->delete();
        return redirect()->route('frontend.company.management')->withFlashSuccess(trans('exceptions.backend.access.companies.deleted'));
    }
}
