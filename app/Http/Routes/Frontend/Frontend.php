<?php

/**
 * Frontend Controllers
 */
Route::get('/', 'FrontendController@index')->name('frontend.index');
Route::get('macros', 'FrontendController@macros')->name('frontend.macros');

/**
 * These frontend controllers require the user to be logged in
 */
Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'User'], function() {
        Route::get('dashboard', 'DashboardController@index')->name('frontend.user.dashboard');
        Route::group(['prefix' => 'users'],function(){
            Route::get('/', 'DashboardController@users')->name('frontend.user.management');
            Route::get('data', 'DashboardController@getUsers')->name('frontend.user.data');
            Route::get('create', 'DashboardController@create')->name('frontend.user.create.form');
            Route::post('create', 'DashboardController@store')->name('frontend.user.create');
            Route::get('edit/{user}', 'DashboardController@edit')->name('frontend.user.edit');
            Route::post('update', 'DashboardController@update')->name('frontend.user.update');
            Route::get('deactivate/{user}', 'DashboardController@deactivate')->name('frontend.user.deactivate');
            Route::get('activate/{user}', 'DashboardController@activate')->name('frontend.user.activate');
            Route::get('delete/{user}', 'DashboardController@delete')->name('frontend.user.delete');
            Route::post('active_company', 'DashboardController@setActiveCompany')->name('frontend.user.active');
        });
        Route::group(['prefix' => 'company'],function(){
            Route::get('/','DashboardController@companies')->name('frontend.company.management');
            Route::get('data','DashboardController@companyData')->name('frontend.company.data');
            Route::get('create','DashboardController@companyCreate')->name('frontend.company.create.form');
            Route::post('create','DashboardController@companyStore')->name('frontend.company.create');
            Route::get('edit/{company}','DashboardController@companyEdit')->name('frontend.company.edit');
            Route::post('update','DashboardController@companyUpdate')->name('frontend.company.update');
            Route::get('delete/{company}','DashboardController@companyDelete')->name('frontend.company.delete');
        });
        Route::get('profile/edit', 'ProfileController@edit')->name('frontend.user.profile.edit');
        Route::patch('profile/update', 'ProfileController@update')->name('frontend.user.profile.update');
    });
});