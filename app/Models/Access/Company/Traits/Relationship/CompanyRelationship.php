<?php

namespace App\Models\Access\Company\Traits\Relationship;

trait CompanyRelationship
{
    /**
     * @return mixed
     */
    public function users()
    {
        return $this->belongsToMany(\App\Models\Access\User\User::class,'company_client','company_id','client_id');
    }
}