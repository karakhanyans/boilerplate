<?php

namespace App\Models\Access\Company;

use App\Models\Access\Company\Traits\Relationship\CompanyRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes,CompanyRelationship;

    protected $fillable = ['name'];
}
